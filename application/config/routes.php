<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|  $route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
| $route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
| $route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route[ 'default_controller' ]  = 'homeController';
$route[ '404_override' ]        = 'myerrorController/not_found';

$route['depositinspection/create'] = 'Deposit_inspection/create';
$route['depositinspection_form_process_create'] = 'Deposit_inspection/create_form_process';
$route['depositinspection'] = 'Deposit_inspection/depositinspection';
$route['depositinspection_datatable'] = 'Deposit_inspection/depositinspection_datatable';
$route['depositinspection/edit/(:any)'] = 'Deposit_inspection/edit';

//new code
$route['update_treecutting_application'] = 'TreeCuttingController/update_treecutting_application';

 
require_once( BASEPATH .'database/DB'. EXT );

$db =& DB();

$query = $db->select('slug,sub_slug,controller,method')
			->where(array('status'=>'1','is_deleted'=>'0'))->get( 'app_routes' );
$result = $query->result();

foreach( $result as $row ) {
	if($row->sub_slug !='') {

		if($row->slug == 'edit') {
			$route[ $row->sub_slug.'/'.$row->slug.'/(:any)' ] = $row->controller.'/'.$row->method.'/$1';
		} elseif($row->slug == 'checklist') {
			$route[ $row->sub_slug.'/'.$row->slug.'/(:any)/(:any)' ] = $row->controller.'/'.$row->method.'/$1/$2';
		} elseif($row->slug == 'edits') {
		    $route[ $row->sub_slug.'/'.$row->slug.'/(:any)' ] = $row->controller.'/'.$row->method.'/$1';
		} elseif($row->slug == 'editss') {
		    $route[ $row->sub_slug.'/'.$row->slug.'/(:any)' ] = $row->controller.'/'.$row->method.'/$1';
		} else {
			$route[ $row->sub_slug.'/'.$row->slug] = $row->controller.'/'.$row->method;
		}
	
	} else {
		$route[$row->slug] = $row->controller.'/'.$row->method;
	}
    
    // $route[ $row->slug.'/:any' ]         = $row->controller;
    // $route[ $row->controller ]           = 'myerrorController/access_denied';
    // $route[ $row->controller.'/:any' ]   = 'myerrorController/access_denied';
}

	// echo'<pre>';print_r($route);exit;

