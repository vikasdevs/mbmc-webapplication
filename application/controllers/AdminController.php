<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/Common.php';
class AdminController extends Common {

	/**
	 * Admin.php
     * @author Vikas Pandey
	 */

    public function __construct() {
        parent::__construct();
        ob_start();
        $this->load->helper(['jwt', 'authorization']);
        $this->load->helper("cookie");
        set_cookie('demo','1');
        set_cookie("email", "");
        set_cookie("password", "");
        set_cookie("user_id", "");
    }

    

    public function addUserDetails() {
        
        extract($_POST);
        $username_check = $this->form_validation
                            ->set_rules('user_name','user_name','required')->run();

        $email_check = $this->form_validation
                        ->set_rules('email_id','email_id','required|valid_email|is_unique[users_table.email_id]')->run();

        $mobile_check = $this->form_validation
                    ->set_rules('user_mobile','user_mobile','required|regex_match[/^[0-9]{10}$/]|is_unique[users_table.user_mobile]')->run();

        $role_check = $this->form_validation
                    ->set_rules('role_id','role_id','required')->run();
        $dept_check = $this->form_validation
                    ->set_rules('dept_id','dept_id','required')->run();

        $pass_check = $this->form_validation
                    ->set_rules('user_mobile','user_mobile','required')->run();
        $data['messg'] = '';

        if(!$username_check || !$email_check || !$mobile_check || !$role_check || !$pass_check || !$dept_check) {
            $data['status'] = '2';
            $data['messg'] = validation_errors();
            // exit;
        } else {
            $extra = array(
                'status' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'password' => $this->hash_password($password));

            $data = array_merge($_POST,$extra);

            unset($data['terms']);

            $result = $this->users_table->insert($data);

            if($result == true) {
                $data['status'] = '1';
                $data['messg'] = 'User Registered successfully.';
            } else {
                $messg = 'Oops! Something went wrong.';
                $data['status'] = '2';
                $data['messg'] = $messg;
            }
        }

        echo json_encode($data);
    }
    
	public function login_view() { 
		$this->load->view('auth/login');
    }

    public function login_check() {
        // Extract user data from POST request
        extract($_POST);
        // print_r($_POST);exit;
        $remember = 0;
        
        if(array_key_exists('remember', $_POST)){
            $remember = 1;
        }
        
        // echo'<pre>';print_r($_POST);exit;
        if(!empty($email_id) && !empty($password)) {

            $userdata = $this->users_table->check_email($email_id, $loginType);

            if(!empty($userdata)) {

                $check_password = $this->check_password($password, $userdata[0]['password']);
                // echo'<pre>';var_dump($check);exit;
                if($check_password != null) {
                    // print_r($userdata);exit;
                    // set session
                    $this->session->set_userdata('user_session',$userdata);
                    
                    if($remember == 1){
                        set_cookie('email', $email_id, time()+ (10 * 365 * 24 * 60 * 60));
                        set_cookie("password", $password, time()+ (10 * 365 * 24 * 60 * 60));
                        set_cookie("user_id", $userdata[0]['user_id'], time()+ (10 * 365 * 24 * 60 * 60));
                    }else{
                        set_cookie("email", "");
                        set_cookie("password", "");
                        set_cookie("user_id", "");
                    }
                    // echo get_cookie('email');exit;

                    $token = AUTHORIZATION::generateToken($userdata);
                    $session_data = array_merge($userdata[0],array('token' => $token));
                    // echo'<pre>';print_r($session_data);exit;
                    // set log
                    $this->user_log($session_data);

                    $data['status'] = '1';
                    $data['messg'] = 'User Logged successfully.';
                    // $data['token'] = $token;
                } else {
                    $data['status'] = '2';
                    $data['messg'] = 'Incorrect password.';
                    // $data['token'] = '';
                }
            } else {
                $data['status'] = '3';
                $data['messg'] = 'Incorrect email address.';
                // $data['token'] = '';
            }

        } else {
            $data['status'] = '4';
            $data['messg'] = 'Incorrect email address .';
            // $data['token'] = '';

        }
        //echo'<pre>';print_r($data);exit;
        echo json_encode($data);
    }

    public function validate_token() {
        extract($_POST);
        // echo'<pre>';print_r($token);exit;
        try {
            // Validate the token
            // Successfull validation will return the decoded user data else returns false
            $data = AUTHORIZATION::validateToken($token);
            if ($data !== false) {
                $response['status'] = '1';
                $response['messg'] = 'Authorized Access!.';
                $response['token'] = $token;
            } else {
                $response['status'] = '2';
                $response['messg'] = 'Unauthorized  Access!.';
                $response['token'] = '';
                $response['data'] = null;
            }

        } catch (Exception $e) {
            // Token is invalid
            // Send the unathorized access message
            $response['status'] = '1';
            $response['messg'] = 'Unauthorized Access!.';
            $response['token'] = '';
        }
        // echo'<pre>';print_r($response);exit;
        echo json_encode($response);
    }

    public function logout() {
        $this->session->unset_userdata('user_session');
        $this->session->unset_userdata('delete_status');
        redirect('login');
    }
    
    //register
	public function register(){
		// echo "kkk";exit;
		$this->load->view('auth/register');
	}
    
}
?>