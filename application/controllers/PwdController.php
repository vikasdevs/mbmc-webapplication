<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'controllers/Common.php';
class PwdController extends Common {

	/**
 	*
 	*/
	public function index()	{
		$dept_id = $this->get_dept_id_by_name('pwd');
		$data['appStatus'] = $this->App_status_level_table->getAllStatusByDept($dept_id);
		// echo'<pre>';print_r($data);exit;
		$this->load->view('applications/pwd/index',$data);
	}

	public function create() {
		$data['app_id'] = $this->get_last_app_id();
		$data['dept_id'] = $this->get_dept_id_by_name('pwd');

		$data['road'] = $this->get_all_road_type();	
		$this->load->view('applications/pwd/create',$data);
	}

	public function edit() {

		$pwd_id = base64_decode($this->uri->segment(3));
		$result = $this->pwd_applications_table->getAllApplicationsDetailsById($pwd_id);
		$request_letter_image = $result['request_letter_id'];
		$geo_map_image = $result['geo_location_map_id'];

		$get_attachments = $this->image_details_table->getImageDetailsById($request_letter_image);
		$request_letter_image_name = array('request_letter_name' => $get_attachments['image_name'], 'request_letter' => $get_attachments['image_path']);

		$geo_map_image = $result['geo_location_map_id'];
		$get_attachments = $this->image_details_table->getImageDetailsById($geo_map_image);
		$geo_map_image_name = array('geo_name' => $get_attachments['image_name'],'geo_location_map' => $get_attachments['image_path']);
		$data['road'] = $this->get_all_road_type();	
		$data['users'] = array_merge($result,$request_letter_image_name,$geo_map_image_name);

		$this->load->view('applications/pwd/edit',$data);
	}

	public function save() {

		extract($_POST);
		// echo'<pre>';print_r($_POST);exit;

		$applicant_name_check = $this->form_validation
            ->set_rules('applicant_name','applicant name','required')->run();

        $applicant_email_id_check = $this->form_validation
                ->set_rules('applicant_email_id','applicant email id','required|valid_email')->run();

        $applicant_mobile_no_check = $this->form_validation
                ->set_rules('applicant_mobile_no','applicant mobile no','required|regex_match[/^[0-9]{10}$/]')->run();

        $applicant_alternate_no_check = $this->form_validation
                ->set_rules('applicant_alternate_no','applicant alternate no','regex_match[/^[0-9]{10}$/]')->run();

        $letter_no_check = $this->form_validation
            ->set_rules('letter_no','letter_no','required')->run();

        $company_name_check = $this->form_validation
                            ->set_rules('company_name','company name','required')->run();

    	$landline_no_check = $this->form_validation
                            ->set_rules('landline_no','landline no','required')->run();

    	$contact_person_check = $this->form_validation
                            ->set_rules('contact_person','contact person','required')->run();

        $road_name_check = $this->form_validation
                            ->set_rules('road_name','road_name','required')->run();

        $road_id_check = $this->form_validation
                            ->set_rules('road_id','road_id','required')->run();

        $start_point_check = $this->form_validation
                            ->set_rules('start_point','start point','required')->run();

        $end_point_check = $this->form_validation
                            ->set_rules('end_point','end point','required')->run();

        $total_length_check = $this->form_validation
                            ->set_rules('total_length','total length','required')->run();

    	$days_of_work_check = $this->form_validation
                            ->set_rules('days_of_work','days of work','required')->run();

        if (!empty($_FILES)) {

            $request_letter_check = $this->form_validation->set_rules('request_letter_name', 'Document', 'required');
		                            
			$geo_location_map_check = $this->form_validation->set_rules('geo_map_name', 'Document', 'required');
        } else {

            $data['status'] = '2';
            $data['messg'] = 'Please choose the documents.';
        }


		if(!$applicant_name_check || !$applicant_email_id_check || !$applicant_mobile_no_check || !$applicant_alternate_no_check || !$letter_no_check || !$company_name_check || !$landline_no_check || !$contact_person_check || !$road_name_check || !$road_id_check || !$start_point_check || !$end_point_check || !$total_length_check || !$days_of_work_check || !$request_letter_check || !$geo_location_map_check) {

            $data['status'] = '2';
            $data['messg'] = validation_errors();
        } else {
        	// echo'<pre>';print_r('hihi');exit;
        	$config['upload_path']   = './uploads/pwd';
	  		$config['allowed_types'] = 'pdf|jpg|png|docx';
	  		$config['max_size']      = '0';
	  		$config['encrypt_name'] = TRUE;

			$this->upload->initialize($config);

            $upload_array = array();
            // echo'<pre>';print_r($_FILES);exit;
            foreach ($_FILES as $key => $files) {
            	// echo'<pre>';print_r($key);exit;
                if(!$this->upload->do_upload($key)) {
                	// echo'sss<pre>';print_r('ss');exit;
                    $data['status'] = '2';
                    $data['messg'] = $this->upload->display_errors();
                    // echo'<pre>';print_r($data);exit;
                } else {
                    $uploaded_data = $this->upload->data();
                    // echo'ss<pre>';print_r($uploaded_data);//exit;
                    $image_data = array(
                        'image_name' => $uploaded_data['orig_name'],
                        'image_enc_name' => $uploaded_data['file_name'],
                        'image_path' => base_url().'uploads/pwd/'.$uploaded_data['file_name'],
                        'image_size' => $uploaded_data['file_size'],
                        'status' => '1',
                        'is_deleted' => '0',
                        'created_at' => date('Y-m-s H:i:s'),
                        'updated_at' => date('Y-m-s H:i:s'),
                    ); 
                    // echo'<pre>';print_r($image_data);exit;
                    $result = $this->upload_files($image_data);
                    // echo'ss<pre>';print_r($result);exit;

                    if($result != null) {
                        $upload_array[$key.'_id'] = $result;
                    } else {
                        $upload_array[] = array($key => $result);
                    }
                }
            }
            // echo'<pre>';print_r($upload_array);exit;


			if(!$this->upload->do_upload("request_letter")) {
				$data['status'] = '2';
	            $data['messg'] = $this->upload->display_errors();
			} else {

				$letter_uploaded_data = $this->upload->data();

				$image_data = array(
					'image_name' => $letter_uploaded_data['orig_name'],
					'image_enc_name' => $letter_uploaded_data['file_name'],
					'image_path' => base_url().'uploads/pwd/'.$letter_uploaded_data['file_name'],
					'image_size' => $letter_uploaded_data['file_size'],
					'status' => '1',
					'is_deleted' => '0',
					'created_at' => date('Y-m-s H:i:s'),
					'updated_at' => date('Y-m-s H:i:s'),
				); 

				$result = $this->upload_files($image_data);
				if($result != null) {
					$request_letter_id = array('request_letter_id' => $result);
				} else {
					$request_letter_id = array('request_letter_id' => $result);
				}
			}

			if(!$this->upload->do_upload("geo_location_map")) {
				$data['status'] = '2';
	            $data['messg'] = $this->upload->display_errors();
			} else {
				$geo_uploaded_data = $this->upload->data();

				$image_data = array(
					'image_name' => $geo_uploaded_data['orig_name'],
					'image_enc_name' => $geo_uploaded_data['file_name'],
					'image_path' => base_url().'uploads/pwd/'.$geo_uploaded_data['file_name'],
					'image_size' => $geo_uploaded_data['file_size'],
					'status' => '1',
					'is_deleted' => '0',
					'created_at' => date('Y-m-s H:i:s'),
					'updated_at' => date('Y-m-s H:i:s'),
				); 

				$result = $this->upload_files($image_data);
				if($result != null) {
					$geo_map_id= array('geo_map_id' => $result);
				} else {
					$geo_map_id = array('geo_map_id' => $result);
				}
			}



		 	$dept_id = $this->department_table->getDepartmentByName('PWD');

			if(!empty($dept_id)) {

				if($id =='') {
					// add form
					$applications_details = array(
						'dept_id' => $dept_id,
						'status' => '1',
						'is_deleted' => '0',
						'created_at' => date('Y-m-s H:i:s'),
						'updated_at' => date('Y-m-s H:i:s'),
					);

					$inserted_app_id = $this->applications_details_table->insert($applications_details);

					$app_id_array = array('app_id' => $inserted_app_id); 
					// unset($_POST['dept_id']);
					unset($_POST['application_no']);

					$extra = array(
		                'status' => '1',
		                'is_deleted' => '0',
		                'created_at' => date('Y-m-d H:i:s'),
		                'updated_at' => date('Y-m-d H:i:s'),
		            );

		            $insert_data = array_merge($_POST,$app_id_array,$upload_array,$extra);
		            
		            unset($insert_data['request_letter_name']);
		            unset($insert_data['geo_map_name']);
		            // echo'<pre>';print_r($insert_data);exit;
					$result = $this->pwd_applications_table->insert($insert_data);
					if($result !=null) {
						$data['status'] = '1';
						$data['messg'] = 'Application added successfully.';
	 				} else {
	 					$data['status'] = '2';
						$data['messg'] = 'Oops! Something went wrong.';
	 				}
				} else {

					// update form
					$applications_details = array(
						'updated_at' => date('Y-m-d H:i:s'),
					);

					$updated_app_result = $this->applications_details_table->update($applications_details,$app_id);

					if($updated_app_result) {
						$extra = array(
			                'updated_at' => date('Y-m-d H:i:s'),
			            );
			            $update_data = array_merge($_POST,$upload_array,$extra);
			            // echo'<pre>';print_r($update_data);exit;
			            unset($update_data['application_no']);
			            unset($update_data['request_letter_name']);
			            unset($update_data['geo_map_name']);
			            // echo'<pre>';print_r($update_data);exit;
						$result = $this->pwd_applications_table->update($update_data,$app_id);

						if($result) {
							$data['status'] = '1';
							$data['messg'] = 'Application updated successfully.';
		 				} else {
		 					$data['status'] = '2';
							$data['messg'] = 'Oops! Something went wrong.';
		 				}

					} else {
						$data['status'] = '2';
						$data['messg'] = 'Oops! Something went wrong.';
					}
				}
			} else {
				$data['status'] = '2';
				$data['messg'] = 'Oops! Something went wrong.';
			}
        }

        echo json_encode($data);
	}

	public function get_lists()	{

		$data = $row = array();
		$session_userdata = $this->session->userdata('user_session');
        $user_id = $session_userdata[0]['user_id'];
        $role_id = $session_userdata[0]['role_id'];

    	$pwdList = $this->pwd_applications_table->getRows($_POST);
    	
        $i = $_POST['start'];

        foreach($pwdList as $pwd){
            $i++;
            $id = $pwd['id'];

            if($pwd['app_id'] != null) {
              	$app_val = 'MBMC-00000'.$pwd['app_id'];
              	$app_no = ++$app_val;
          	} else {
            	$app_no = 'MBMC-000001';
          	}

            $application_no = $app_no;
            $applicant_name = $pwd['applicant_name'];
            $applicant_email_id = $pwd['applicant_email_id'];
            $applicant_mobile_no = $pwd['applicant_mobile_no'];
            $applicant_alternate_no = $pwd['applicant_alternate_no'];
            $applicant_address = $pwd['applicant_address'];
            $company_name = $pwd['company_name'];
			$landline_no = $pwd['landline_no'];
            $contact_person = $pwd['contact_person'];
            $letter_no = $pwd['letter_no'];
            $letter_date = $pwd['letter_date'];
            $road_name = $pwd['road_name'];
            $start_point = $pwd['start_point'];
            $end_point = $pwd['end_point'];
            $total_length = $pwd['total_length'];
            $days_of_work = $pwd['days_of_work'];
            $status = $pwd['status'];

            $road_details = $this->road_type_table->getRoadDetailsById($pwd['road_id']);
            // echo'<pre>';print_r($road_details);exit;
            if($road_details != null) {
            	$road_type = $road_details['road_title'];
            } else {
            	$road_type = 'NA';
            }

            // $request_letter_id = $pwd['request_letter_id'];
            
            // $request_letter_image = $this->image_details_table->getImageDetailsById($pwd['request_letter_id']);
            // $request_letter_button = '<a type="button" data-name="Request Letter" data-path="'.$request_letter_image['image_path'].'" data-toggle="modal" onclick="get_image(this)" data-target="#modal-image" class="btn btn-block btn-success white">'.$request_letter_image['image_name'].'</a>';



            // $geo_map_id_image = $this->image_details_table->getImageDetailsById($pwd['geo_map_id']);



            // $geo_map_id_image = $this->image_details_table->getImageDetailsById($pwd['geo_map_id']);



            // $geo_map_id_button = '<a type="button" data-name="Geo Map" data-toggle="modal" data-path="'.$geo_map_id_image['image_path'].'" onclick="get_image(this)" data-target="#modal-image" class="btn btn-block btn-danger white">'.$geo_map_id_image['image_name'].'</a>';

            $dept_id = $this->applications_details_table->getDeptId($pwd['app_id'])['dept_id'];
            $status_details = $this->App_status_level_table->getAllStatusById($status);
            // echo'ss<pre>';print_r($status_details);exit;
            if($status_details != null) {
            	$status_type = $status_details[0]['status_type'];
            	$status_title = $status_details[0]['status_title'];
            	if($status_type == '1') {
	            	$class = "btn-danger white";
	            } elseif($status == '2') {
	            	$class = "btn-success white";
	            } 

	            $status ='<a type="button" data-toggle="modal"  data-pwd="'.$pwd['id'].'" data-app="'.$pwd['app_id'].'" data-user="'.$user_id.'" data-role="'.$role_id.'" data-dept="'.$dept_id.'" data-status="'.$status.'" data-target="#modal-status" class="status_button btn btn-sm btn-danger white">'.$status_title.'</a>';
            } else {
            	$status ='NA';
            }
            
            $remarks = '<a type="button" data-toggle="modal" data-pwd="'.$pwd['id'].'" data-app="'.$pwd['app_id'].'"  data-target="#modal-remarks" class="remarks_button btn btn-sm btn-primary white">Remarks</a>';

            $action = '<a aria-label="View documents" data-microtip-position="top" role="tooltip" type="button" data-toggle="modal"
                        data-image = "'.$pwd['request_letter_id'].','.$pwd['geo_location_map_id'].'" data-geo = "'.$pwd['geo_location_map_id'].'" data-pwd="'.$pwd['id'].'" 
                        data-app="'.$pwd['app_id'].'"  
                        data-target="#modal-doc" 
                        class="anchor nav-link-icon doc_button">
                        <i class=" nav-icon fas fa-file"></i>
                        </a>
                <a aria-label="Edit" data-microtip-position="top" role="tooltip" href="'.base_url().'pwd/edit/'.base64_encode($id).'" class="nav-link-icon">
              		        <i class="nav-icon fas fa-edit"></i>
                        </a>';
            // echo'<pre>';print_r($request_letter_image);exit;

            $documents = '';


            $documents = '<a type="button" data-toggle="modal" data-image = "'.$pwd['request_letter_id'].','.$pwd['geo_location_map_id'].'" data-geo = "'.$pwd['geo_location_map_id'].'" data-pwd="'.$pwd['id'].'" data-app="'.$pwd['app_id'].'"  data-target="#modal-doc" class="doc_button btn btn-block btn-info white">Docs</a>';

            $documents = '<a type="button" data-toggle="modal" data-image = "'.$pwd['request_letter_id'].','.$pwd['geo_location_map_id'].'" data-geo = "'.$pwd['geo_location_map_id'].'" data-pwd="'.$pwd['id'].'" data-app="'.$pwd['app_id'].'"  data-target="#modal-doc" class="doc_button btn btn-block btn-info white">Docs</a>';


            // $request_letter_button .''.$geo_map_id_button;

            // $data[] = array($i,$application_no, $applicant_name, $applicant_email_id, $applicant_mobile_no,
            // 	$applicant_alternate_no,$applicant_address,$letter_no,$letter_date,$company_name,$landline_no,$contact_person,$road_name,$road_type,$start_point,$end_point,$total_length,$days_of_work,$documents,$remarks,$status,$action);

            // $data[] = array($i,$application_no, $applicant_name, $applicant_email_id, $applicant_mobile_no,
            // 	$applicant_address,$letter_no,$letter_date,$company_name,$landline_no,$contact_person,$days_of_work,$documents,$remarks,$status,$action);

            $data[] = array($i,$application_no, $applicant_name, $applicant_email_id, $applicant_mobile_no,
            	$company_name,$days_of_work,$remarks,$status,$action);
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->pwd_applications_table->countAll(),
            "recordsFiltered" => $this->pwd_applications_table->countFiltered($_POST),
            "data" => $data,
        );
        
        echo json_encode($output);
	}

	public function add_remarks() {
		extract($_POST);
		$remarks_check = $this->form_validation
                            ->set_rules('remarks','remarks','required')->run();
        $status_check = $this->form_validation
                            ->set_rules('status','status','required')->run();
        if(!$remarks_check) {
        	$data['status'] = '2';
        	$data['messg'] = 'Please provide the remarks.';
        } else {

    	 	$session_userdata = $this->session->userdata('user_session');
	        $user_id = $session_userdata[0]['user_id'];
	        $role_id = $session_userdata[0]['role_id'];
        	$insert_data = array(
        		'app_id' => $app_id,
        		'dept_id' => $dept_id,
        		'user_id' => $user_id,
                'role_id' => $role_id,
        		'remarks'=> $remarks, 
                'status_id' => $status,
                'status' => '1',
                'is_deleted' => '0',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-s H:i:s'),
            );

			$result = $this->application_remarks_table->insert($insert_data);
			if($result != null) {

				$update_data = array(
					'status' => $status,
					'updated_at' => date('Y-m-d H:i:s')
				);

				$final_result = $this->pwd_applications_table->update($update_data,$app_id);

				if($final_result != null) {
					$data['status'] = '1';
        			$data['messg'] = 'Remark updated successfully.';

				} else {
					$data['status'] = '2';
        			$data['messg'] = 'Oops! Something went wrong.';
				}
			} else {
				$data['status'] = '2';
        		$data['messg'] = 'Oops! Something went wrong.';

			}
        }
        echo json_encode($data);
	}
}
