<?php 

function file_upload_config()
{
	$config['upload_path']          = './uploads/marriage/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg|webp';
    $config['encrypt_name']            = TRUE;
    return $config;
}

function reconfig_file_structure($file_data,$key)
{

	if (!empty($file_data['name'][$key])) {
		$oneTreeImage['name'] = $file_data['name'][$key];
		$oneTreeImage['type'] = $file_data['type'][$key];
		$oneTreeImage['tmp_name'] = $file_data['tmp_name'][$key];
		$oneTreeImage['error'] = $file_data['error'][$key];
		$oneTreeImage['size'] = $file_data['size'][$key];
		return $oneTreeImage ;
	} else {
		return FALSE ;
	}
}