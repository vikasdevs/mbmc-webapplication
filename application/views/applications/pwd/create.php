  <?php $this->load->view('includes/header'); ?>

  <!-- Main Sidebar Container -->
  <?php $this->load->view('includes/sidenav'); ?>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" rel="stylesheet"/>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Application</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Add Application</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid - ->
    </section> -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <!-- /.card-header -->
            <div class="row">
              <div class="col-12">
                <div class="card card-primary">
                  <form role="form" class="pwd-form" id="pwd-form" method="post" enctype="multipart/form-data">
                    <div class="card-header">
                       <div class="row">
                        <div class="col-12">
                          <h3 class="card-title">
                            <label for="email_id" class="text-info">Personal Information</label>
                          </h3>
                        </div>
                      </div>
                    </div>
                    <div class="card-body">
                      <div class="row">
                        <div class="col-4">
                          <div class="form-group">
                            <label for="application_no"><span>Application No</span><span class="red">*</span></label>
                            <input type="hidden" value="<?=($app_id != null) ? $app_id :'1' ?>" name="app_id" id="app_id">
                             <input type="hidden" value="" name="id" id="id">
                            <?php
                              if($app_id != null) {
                                  $app_val = 'MBMC-00000'.$app_id;
                                  $app_no = ++$app_val;
                              } else {
                                $app_no = 'MBMC-000001';
                              }
                           ?>
                            <input type="text" class="form-control" value="<?=$app_no; ?>" name="application_no" id="application_no" placeholder="Enter Application no" readonly>
                          </div>
                        </div>
                        <div class="col-4">
                          <div class="form-group">
                            <label for="applicant_name">Applicant Name<span class="red">*</span></label>
                            <input type="text" class="form-control" name="applicant_name" id="applicant_name" placeholder="Enter Applicant name">
                          </div>
                        </div>
                        <div class="col-4">
                          <div class="form-group">
                            <label for="email_id">Applicant Email Id<span class="red">*</span></label>
                            <input type="text" class="form-control" name="applicant_email_id" id="applicant_email_id" placeholder="Enter email Id">
                          </div>
                        </div>
                        <div class="col-4">
                          <div class="form-group">
                            <label for="mobile_no">Applicant Mobile no<span class="red">*</span></label>
                            <input type="text" class="form-control" name="applicant_mobile_no" id="applicant_mobile_no" placeholder="Enter mobile no">
                          </div>
                        </div>
                        <div class="col-4">
                          <div class="form-group">
                            <label  for="alert_mobile_no">Alternate Mobile no<span class="grey">&nbsp;(optional)</span></label>
                            <input type="text" class="form-control" name="applicant_alternate_no" id="applicant_alternate_no" placeholder="Enter alternate mobile no">
                          </div>
                        </div>
                        <div class="col-4">
                          <div class="form-group">
                            <label for="alert_mobile_no">Applicant Address<span class="red">*</span></label>
                            <textarea type="text" class="form-control" name="applicant_address" id="applicant_address" placeholder="Enter applicant address"></textarea> 
                          </div>
                        </div>
                      </div>
                    </div>

                    <!-- company info -->
                    <div class="card-header">
                      <h3 class="card-title">
                        <label for="email_id" class="text-info">Company Information</label>
                      </h3>
                    </div>
                    <div class="card-body">
                      <div class="row">
                        <div class="col-4">
                          <div class="form-group">
                            <label for="letter_no">Letter No<span class="grey">&nbsp;(optional)</span></label>
                            <input type="text" class="form-control" name="letter_no" id="letter_no" placeholder="Enter letter no">
                          </div>
                        </div>
                        <div class="col-4">
                          <div class="form-group">
                            <label for="dol">Date on Letter<span class="grey">&nbsp;(optional)</span></label>
                            <input type="text" class="form-control datepicker" name="letter_date" id="letter_date" placeholder="Enter Date on Letter">
                          </div>
                        </div>
                        
                        <div class="col-4">
                          <div class="form-group">
                            <label for="company_name">Company Name<span class="red">*</span></label>
                            <input type="text" class="form-control" name="company_name" id="company_name" placeholder="Enter company name">
                          </div>
                        </div>
                        <div class="col-4">
                          <div class="form-group">
                            <label for="exampleCheck1">Landline No<span class="red">*</span></label>
                            <input type="text" class="form-control" name="landline_no" id="landline_no" placeholder="Enter landline no">
                          </div>
                        </div>
                        <div class="col-4">
                          <div class="form-group">
                            <label for="contact_person">Name of Contact Person<span class="red">*</span></label>
                            <input type="text" name="contact_person" class="form-control" id="contact_person" placeholder="Enter Name of contact person">
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="card-header">
                      <h3 class="card-title">
                        <label for="email_id" class="text-info">Road Information</label>
                      </h3>
                    </div>
                    <div class="card-body">
                      <div class="row">
                        <div class="col-4">
                          <div class="form-group">
                            <label for="name_of_road">Name of Road <span class="red">*</span></label>
                            
                            <input type="text" class="form-control" name="road_name" id="road_name" placeholder="Enter name of road">
                          </div>
                        </div>
                        <div class="col-4">
                          <div class="form-group">
                            <label for="road_type">Road Type<span class="red">*</span></label>
                            <select class="selectpicker form-control" id="road_id" name="road_id" data-live-search="true">
                              <option value="">---Select Road Type---</option>
                              <?php
                                foreach ($road as $key => $val) {
                                  echo '<option value="'.$val['road_id'].'">'.$val['road_title'].'</option>';
                                }
                              ?>
                            </select>
                          </div>
                        </div>
                        <div class="col-4">
                          <div class="form-group">
                            <label for="start_point">Start Point<span class="red">*</span></label>
                            <input type="text" class="form-control" name="start_point" id="start_point" placeholder="Enter start point">
                            
                          </div>
                        </div>
                        <div class="col-4">
                          <div class="form-group">
                            <label for="end_point">End Point<span class="red">*</span></label>
                            <input type="text" class="form-control" name="end_point" id="end_point" placeholder="Enter enter end point">
                            
                          </div>
                        </div>

                        <div class="col-4">
                          <div class="form-group">
                            <label for="total_length">Total Length<span class="red">*</span></label>
                            <input type="text" class="form-control" name="total_length" id="total_length" placeholder="Total length">
                          </div>
                        </div>

                        <div class="col-4">
                          <div class="form-group">
                            <label for="dow">Days of work<span class="red">*</span></label>
                            <input type="text" class="form-control" name="days_of_work" id="days_of_work"  placeholder="Enter the Days of work">
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="card-header">
                      <h3 class="card-title">
                        <label for="email_id" class="text-info">Attachments</label>
                      </h3>
                    </div>

                    <div class="card-body">
                      <div class="row">
                        <div class="col-6">
                          <div class="form-group">
                            <label for="request_letter">Request Letter<span class="red">*</span></label>
                            <div class="form-group">
                              <div class="custom-file">
                                <input type="file" name="request_letter_name" id="request_letter" class="custom-file-input">
                                <label class="custom-file-label" for="request_letter">Choose file</label>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-6">
                          <h3 class="card-title link-margin">
                            <label for="" id="request_letter_name" class="text-info"> Please select a document</label>
                            <input type="hidden" name="request_letter_name" id="request_letter_name_id">
                          </h3>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-6">
                          <div class="form-group">
                            <label for="geo_location_map">Geo Location map<span class="red">*</span></label>
                            <div class="form-group">
                              <div class="custom-file">
                                <input type="file" name="geo_location_map" id="geo_location_map" class="custom-file-input">
                                <label class="custom-file-label" for="geo_location_map">Choose file</label>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-6" style="">
                          <h3 class="card-title link-margin">
                            <label for="" id="geo_map_name"  class="text-info"> Please select a document </label>
                            <input type="hidden" name="geo_map_name" id="geo_map_name_id">
                          </h3>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-12">
                            <h3 class="card-title">
                              <label for="email_id" class="text-danger">Note:</label>
                              <ul style="list-style: none;">
                                <li>
                                  <i class=" text-danger fas fa-exclamation-circle"></i>
                                  <span class="text-danger">Only JPG, JPEG, PNG, PDF, DOCX are allowed.</span>
                                </li>
                                <li>
                                  <i class="text-danger fas fa-exclamation-circle"></i>
                                  <span class="text-danger" >File size should Not be more than 5 MB.</span>
                                  
                                </li>
                              </ul>
                            </h3>
                        </div>
                      </div>
                    </div>
                    <div class="card-footer">
                       <div class="row center">
                         <div class="col-12">
                            <a href="<?= base_url()?>pwd" class="btn btn-lg btn-info white">Cancel</a>
                            <button type="submit" class="btn btn-lg btn-primary right">Submit</button>
                        </div>
                      </div>
                      
                    </div>
                  </form>
                </div>  
              </div>
        </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
   <?php $this->load->view('includes/footer');?>

  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- DataTables -->
<script src="<?php echo base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url()?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url()?>assets/dist/js/demo.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>/assets/custom/js/applications.js" id = "createPwd" is_user = "<?= $this->authorised_user['is_user']; ?>"></script>
<script type="text/javascript">
  $('#request_letter').change(function() {
    var file = $('#request_letter')[0].files[0].name;
    $('#request_letter_name').text(file);
    $('#request_letter_id').val(file);
  });

  $('#geo_location_map').change(function() {
    var file = $('#geo_location_map')[0].files[0].name;
    $('#geo_map_name').text(file);
    $('#geo_map_name_id').val(file);
  });
</script>

<!-- page script -->
</body>
</html>
