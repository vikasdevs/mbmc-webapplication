-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 01, 2020 at 09:44 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mbmc`
--

-- --------------------------------------------------------

--
-- Table structure for table `applications_details`
--

CREATE TABLE `applications_details` (
  `application_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applications_details`
--

INSERT INTO `applications_details` (`application_id`, `dept_id`, `status`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `application_remarks`
--

CREATE TABLE `application_remarks` (
  `id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `remarks` longtext NOT NULL,
  `status_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `application_remarks`
--

INSERT INTO `application_remarks` (`id`, `app_id`, `dept_id`, `user_id`, `role_id`, `remarks`, `status_id`, `status`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 3, 'test', 2, 1, 0, '2020-03-27 14:42:51', '0000-00-00 00:00:00'),
(2, 1, 1, 1, 3, 'test', 5, 1, 0, '2020-03-27 14:44:43', '0000-00-00 00:00:00'),
(3, 1, 1, 1, 3, 'test', 5, 1, 0, '2020-03-27 14:46:28', '2020-03-28 14:46:28'),
(4, 1, 1, 1, 3, 'test', 5, 1, 0, '2020-03-27 14:46:44', '0000-00-00 00:00:00'),
(5, 1, 1, 1, 3, 'test', 5, 1, 0, '2020-03-27 14:47:05', '2020-03-05 14:47:05'),
(6, 1, 1, 1, 3, 'test', 5, 1, 0, '2020-03-27 14:49:18', '2020-03-18 14:49:18'),
(7, 1, 1, 1, 3, 'test', 5, 1, 0, '2020-03-27 14:49:31', '2020-03-31 14:49:31'),
(8, 1, 1, 1, 3, 'test', 2, 1, 0, '2020-03-27 14:49:57', '0000-00-00 00:00:00'),
(9, 1, 1, 1, 3, 'test', 2, 1, 0, '2020-03-27 14:50:30', '2020-03-30 14:50:30'),
(10, 1, 1, 1, 3, 'test', 5, 1, 0, '2020-03-27 14:55:10', '2020-03-10 14:55:10'),
(11, 1, 1, 1, 3, 'test', 1, 1, 0, '2020-03-30 12:40:53', '0000-00-00 00:00:00'),
(12, 1, 1, 1, 3, 'test', 2, 1, 0, '2020-03-30 12:41:40', '0000-00-00 00:00:00'),
(13, 1, 1, 1, 8, 'verified', 4, 1, 0, '2020-03-30 12:43:18', '2020-03-18 12:43:18');

-- --------------------------------------------------------

--
-- Table structure for table `app_premssion`
--

CREATE TABLE `app_premssion` (
  `id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `app_title` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `app_routes`
--

CREATE TABLE `app_routes` (
  `id` bigint(20) NOT NULL,
  `slug` varchar(192) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sub_slug` varchar(255) NOT NULL,
  `controller` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `method` varchar(255) NOT NULL,
  `short_desc` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `app_routes`
--

INSERT INTO `app_routes` (`id`, `slug`, `sub_slug`, `controller`, `method`, `short_desc`, `created_at`, `status`, `is_deleted`) VALUES
(1, 'login', '', 'adminController', 'login_view', 'login page', '2020-03-19 17:17:09', 1, 0),
(2, 'add', 'users', 'usersController', 'add', 'add', '2020-03-19 17:17:15', 1, 0),
(3, 'login_check', '', 'adminController', 'login_check', 'login logic', '2020-03-19 17:17:18', 1, 0),
(4, 'addusers', '', 'adminController', 'addUserDetails', 'registration logic', '2020-03-19 17:17:21', 1, 0),
(5, 'roles', '', 'rolesController', 'index', 'roles data', '2020-03-19 17:17:21', 1, 0),
(6, 'getlist', 'roles', 'rolesController', 'get_lists', 'roles data table', '2020-03-19 17:17:21', 1, 0),
(7, 'save', 'roles', 'rolesController', 'save', 'save role', '2020-03-19 17:17:21', 1, 0),
(8, 'update', 'roles', 'rolesController', 'update', 'update role', '2020-03-19 17:17:21', 1, 0),
(9, 'applications', '', 'ApplicationsController', 'index', 'applications data', '2020-03-19 17:17:21', 1, 0),
(10, 'pwd', '', 'PwdController', 'index', 'Pwd data', '2020-03-19 17:17:21', 1, 0),
(11, 'create', 'pwd', 'PwdController', 'create', 'Pwd create', '2020-03-19 17:17:21', 1, 0),
(12, 'departments', '', 'departmentsController', 'index', 'department data', '2020-03-19 17:17:21', 1, 0),
(13, 'getlist', 'dept', 'departmentsController', 'get_lists', 'department data table', '2020-03-19 17:17:21', 1, 0),
(14, 'save', 'dept', 'departmentsController', 'save', 'save dept', '2020-03-19 17:17:21', 1, 0),
(15, 'update', 'dept', 'departmentsController', 'update', 'update dept', '2020-03-19 17:17:21', 1, 0),
(16, 'add', 'pwd', 'PwdController', 'add', 'Pwd add', '2020-03-19 17:17:21', 1, 0),
(17, 'getlist', 'pwd', 'PwdController', 'get_lists', 'Pwd data table', '2020-03-19 17:17:21', 1, 0),
(18, '403', 'error', 'MyerrorController', 'access_denied', 'access denied', '2020-03-19 17:17:21', 1, 0),
(19, 'logout', '', 'adminController', 'logout', 'logout page', '2020-03-19 17:17:09', 1, 0),
(20, 'addremarks', 'pwd', 'PwdController', 'add_remarks', 'Pwd add remarks', '2020-03-19 17:17:21', 1, 0),
(21, 'getStatusByDeptRole', 'status', 'StatusController', 'get_status_by_dept_role', 'status', '2020-03-19 17:17:21', 1, 0),
(22, 'users', '', 'usersController', 'index', 'users data', '2020-03-19 17:17:21', 1, 0),
(23, 'getlist', 'users', 'usersController', 'get_lists', 'view', '2020-03-19 17:17:21', 1, 0),
(24, 'update', 'users', 'usersController', 'update', 'update', '2020-03-19 17:17:21', 1, 0),
(25, 'edit', 'users', 'usersController', 'edit', 'edit', '2020-03-19 17:17:15', 1, 0),
(26, 'save', 'users', 'usersController', 'save', 'save', '2020-03-19 17:17:15', 1, 0),
(27, 'remarksbyid', 'remarks', 'RemarksController', 'get_app_remarks_by_id', 'app remarks', '2020-03-19 17:17:15', 1, 0),
(28, 'road', '', 'roadController', 'index', 'roaddata', '2020-03-19 17:17:21', 1, 0),
(29, 'getlist', 'road', 'roadController', 'get_lists', 'road data table', '2020-03-19 17:17:21', 1, 0),
(30, 'save', 'road', 'roadController', 'save', 'save road', '2020-03-19 17:17:21', 1, 0),
(31, 'update', 'road', 'roadController', 'update', 'update road', '2020-03-19 17:17:21', 1, 0),
(32, 'edit', 'pwd', 'PwdController', 'edit', 'Pwd edit', '2020-03-19 17:17:21', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `app_status_level`
--

CREATE TABLE `app_status_level` (
  `status_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `status_title` varchar(255) NOT NULL,
  `status_type` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `upadate_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_status_level`
--

INSERT INTO `app_status_level` (`status_id`, `dept_id`, `role_id`, `status_title`, `status_type`, `status`, `is_deleted`, `created_at`, `upadate_at`) VALUES
(1, 1, 3, 'pending from clerk', 1, 1, 0, '2020-03-27 00:00:00', '2020-03-27 00:00:00'),
(2, 1, 3, 'Verified by clerk', 2, 1, 0, '2020-03-27 00:00:00', '2020-03-27 00:00:00'),
(3, 1, 8, 'Pending from Jr.engineer', 1, 1, 0, '2020-03-27 00:00:00', '2020-03-27 00:00:00'),
(4, 1, 8, 'Verified by Jr.Engineer', 2, 1, 0, '2020-03-27 00:00:00', '2020-03-27 00:00:00'),
(5, 1, 3, 'Rejected by clerk', 1, 1, 0, '2020-03-27 00:00:00', '2020-03-27 00:00:00'),
(6, 1, 8, 'Approved by Jr. Engineer', 2, 1, 0, '2020-03-27 00:00:00', '2020-03-27 00:00:00'),
(7, 1, 3, 'Approved by clerk', 1, 1, 0, '2020-03-27 00:00:00', '2020-03-27 00:00:00'),
(8, 1, 8, 'Rejected by Jr.Engineer', 1, 1, 0, '2020-03-27 00:00:00', '2020-03-27 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `auth_sessions`
--

CREATE TABLE `auth_sessions` (
  `id` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `token` varchar(255) NOT NULL,
  `login_time` datetime DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `browser` varchar(255) NOT NULL,
  `browser_version` varchar(255) NOT NULL,
  `os` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_sessions`
--

INSERT INTO `auth_sessions` (`id`, `user_id`, `token`, `login_time`, `ip_address`, `browser`, `browser_version`, `os`, `created_at`) VALUES
(1, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-19 13:25:28'),
(2, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-19 13:27:16'),
(3, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-19 13:28:48'),
(4, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-19 13:36:52'),
(5, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-19 13:37:15'),
(6, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-19 13:38:01'),
(7, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-19 13:38:28'),
(8, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-19 13:39:06'),
(9, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-19 13:39:57'),
(10, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-19 13:44:56'),
(11, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-19 13:49:37'),
(12, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-19 13:50:03'),
(13, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-19 13:51:23'),
(14, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-19 13:51:34'),
(15, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-19 13:52:27'),
(16, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-19 13:53:03'),
(17, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-19 14:08:04'),
(18, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-19 14:09:26'),
(19, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-19 14:10:16'),
(20, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-19 14:10:48'),
(21, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-19 14:22:40'),
(22, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-20 06:16:57'),
(23, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-20 06:19:11'),
(24, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-20 06:25:47'),
(25, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-20 06:28:49'),
(26, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-20 06:30:37'),
(27, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-20 06:36:08'),
(28, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-20 07:56:52'),
(29, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-20 11:17:50'),
(30, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-20 11:38:35'),
(31, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-20 11:41:12'),
(32, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-20 11:41:34'),
(33, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-20 11:55:54'),
(34, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-20 12:36:33'),
(35, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.132', 'Windows 10', '2020-03-21 10:44:16'),
(36, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.132', 'Windows 10', '2020-03-23 07:34:23'),
(37, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-26 14:08:34'),
(38, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-27 08:36:40'),
(39, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-27 08:36:50'),
(40, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-27 08:37:05'),
(41, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-27 10:43:29'),
(42, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-27 11:02:54'),
(43, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMSIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-27 11:03:10'),
(44, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMyIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-27 11:28:47'),
(45, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMyIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-27 11:29:15'),
(46, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMyIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-27 11:29:54'),
(47, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMyIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-27 11:31:00'),
(48, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMyIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-27 11:33:29'),
(49, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiOCIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-27 13:53:52'),
(50, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiOCIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-27 13:54:33'),
(51, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMyIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-27 14:00:43'),
(52, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMyIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwicGFzc3dvcmQiOiIkMmEkMDgkWUhJV0tXbmVhRDZaeEdHY2', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-30 07:07:00'),
(53, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMyIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwiZGVwdF9pZCI6IjEiLCJwYXNzd29yZCI6IiQyYSQwOCRZSE', '2020-03-19 07:35:17', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-30 10:33:48'),
(54, 7, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjciLCJyb2xlX2lkIjoiMiIsImVtYWlsX2lkIjoiYmlqZW5kcmEubWlzaHJhQGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoiYmlqZW5kcmEgbWlzaHJhIiwidXNlcl9tb2JpbGUiOiI4NzU0OTYxMjcyIiwiZGVwdF9pZCI6IjEiLCJwYXNzd29yZCI6IiQyYS', '2020-03-30 09:17:28', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-30 10:34:07'),
(55, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiMyIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwiZGVwdF9pZCI6IjIiLCJwYXNzd29yZCI6IiQyYSQwOCRZSE', '2020-03-30 12:29:04', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-30 12:39:53'),
(56, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiOCIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwiZGVwdF9pZCI6IjIiLCJwYXNzd29yZCI6IiQyYSQwOCRZSE', '2020-03-30 12:29:04', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-03-30 12:42:15'),
(57, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.W3sidXNlcl9pZCI6IjEiLCJyb2xlX2lkIjoiOCIsImVtYWlsX2lkIjoidmlrYXMucGFuZGV5QGFhcmF2c29mdHdhcmUuY29tIiwidXNlcl9uYW1lIjoidmlrYXMgcGFuZGV5IiwidXNlcl9tb2JpbGUiOiI4MTY5Mzk2MzU1IiwiZGVwdF9pZCI6IjIiLCJwYXNzd29yZCI6IiQyYSQwOCRZSE', '2020-03-30 12:29:04', '::1', 'Chrome', '80.0.3987.149', 'Windows 10', '2020-04-01 07:57:29');

-- --------------------------------------------------------

--
-- Table structure for table `department_table`
--

CREATE TABLE `department_table` (
  `dept_id` int(11) NOT NULL,
  `dept_title` varchar(255) NOT NULL,
  `dept_desc` text NOT NULL,
  `status` tinyint(4) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `department_table`
--

INSERT INTO `department_table` (`dept_id`, `dept_title`, `dept_desc`, `status`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'PWD', 'Public welfare department', 1, 0, '2020-03-21 00:00:00', '2020-03-30 12:32:21'),
(2, 'Storage', 'Storage department', 2, 0, '2020-03-21 00:00:00', '2020-03-30 12:27:28'),
(3, 'Tree Cutting', 'Tree', 1, 0, '2020-03-23 14:02:44', '2020-03-23 14:02:44'),
(4, 'Tree Cutting', 'Tree', 1, 0, '2020-03-23 14:03:41', '2020-03-23 14:03:41'),
(5, 'Medical Health', 'medical ', 1, 0, '2020-03-30 12:27:12', '2020-03-30 12:27:12');

-- --------------------------------------------------------

--
-- Table structure for table `image_details`
--

CREATE TABLE `image_details` (
  `image_id` int(11) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `image_enc_name` varchar(255) NOT NULL,
  `image_path` text NOT NULL,
  `image_size` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `image_details`
--

INSERT INTO `image_details` (`image_id`, `image_name`, `image_enc_name`, `image_path`, `image_size`, `status`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'AC_Statment.pdf', '12fd81186080a4e937193facd1fb7c43.pdf', 'http://localhost/mbmc/uploads/pwd/12fd81186080a4e937193facd1fb7c43.pdf', '65.17', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Avinash__Pandey__Resume.docx', '6a443c941fbf5d3dea14ea368acd8b74.docx', 'http://localhost/mbmc/uploads/pwd/6a443c941fbf5d3dea14ea368acd8b74.docx', '21.07', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'AC_Statment.pdf', '28511ee3166856f9846b0577fdecf140.pdf', 'http://localhost/mbmc/uploads/pwd/28511ee3166856f9846b0577fdecf140.pdf', '65.17', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Avinash__Pandey__Resume.docx', '1f608c56d7891fc7056c93a8baad64f0.docx', 'http://localhost/mbmc/uploads/pwd/1f608c56d7891fc7056c93a8baad64f0.docx', '21.07', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `pwd_applications`
--

CREATE TABLE `pwd_applications` (
  `id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL,
  `applicant_name` varchar(255) NOT NULL,
  `applicant_email_id` varchar(255) NOT NULL,
  `applicant_mobile_no` varchar(11) NOT NULL,
  `applicant_alternate_no` varchar(255) NOT NULL,
  `applicant_address` longtext NOT NULL,
  `company_name` text NOT NULL,
  `landline_no` varchar(255) NOT NULL,
  `contact_person` varchar(255) NOT NULL,
  `letter_no` varchar(255) NOT NULL,
  `letter_date` varchar(255) NOT NULL,
  `road_name` text NOT NULL,
  `start_point` varchar(255) NOT NULL,
  `end_point` varchar(255) NOT NULL,
  `total_length` varchar(255) NOT NULL,
  `road_id` int(11) NOT NULL,
  `days_of_work` varchar(255) NOT NULL,
  `request_letter_id` int(11) NOT NULL,
  `geo_map_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pwd_applications`
--

INSERT INTO `pwd_applications` (`id`, `app_id`, `applicant_name`, `applicant_email_id`, `applicant_mobile_no`, `applicant_alternate_no`, `applicant_address`, `company_name`, `landline_no`, `contact_person`, `letter_no`, `letter_date`, `road_name`, `start_point`, `end_point`, `total_length`, `road_id`, `days_of_work`, `request_letter_id`, `geo_map_id`, `status`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 1, 'vikas pandey', 'vikas@gmail.com', '8169396355', '8097876819', 'Jogeshwari West Mumbai 400102', 'inkpot pvt ltd', '0200451256', 'abc', '1234`', '03/10/2020', 'Sv road', '123', '134', '142', 1, '152', 1, 2, 4, 0, '0000-00-00 00:00:00', '2020-03-30 12:43:18'),
(2, 2, 'shashikant pandey', 'shashikant@gmail.com', '9004189508', '8097876818', 'Behram Baug', 'inkpot pvt ltd', '12345678', 'vikash', '123', '03/17/2020', 'SV road', '123', '111', '123', 1, '152', 3, 4, 5, 0, '2020-03-26 12:26:49', '2020-03-27 14:47:05');

-- --------------------------------------------------------

--
-- Table structure for table `road_type`
--

CREATE TABLE `road_type` (
  `road_id` int(11) NOT NULL,
  `road_title` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `road_type`
--

INSERT INTO `road_type` (`road_id`, `road_title`, `status`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'asphalt', 1, 0, '2020-03-24 00:00:00', '2020-03-30 12:11:26'),
(2, 'concrete', 1, 0, '2020-03-24 00:00:00', '2020-03-30 12:11:31'),
(3, 'Paver', 1, 0, '2020-03-30 12:13:42', '2020-03-30 12:13:42'),
(4, 'earth', 1, 0, '2020-03-30 12:14:04', '2020-03-30 12:14:04');

-- --------------------------------------------------------

--
-- Table structure for table `roles_table`
--

CREATE TABLE `roles_table` (
  `role_id` int(11) NOT NULL,
  `role_title` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `roles_table`
--

INSERT INTO `roles_table` (`role_id`, `role_title`, `status`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'Superadmin', 1, 0, '2020-03-18 15:58:26', '2020-03-30 11:51:01'),
(2, 'subadmin', 1, 0, '2020-03-18 15:58:26', '2020-03-21 12:42:50'),
(3, 'clerk', 1, 0, '2020-03-18 15:58:26', '2020-03-21 12:46:25'),
(4, 'comm', 1, 0, '2020-03-21 12:14:17', '2020-03-21 12:46:27'),
(5, 'commm', 1, 0, '2020-03-21 12:18:25', '2020-03-21 12:46:30'),
(6, 'dy.comm', 1, 0, '2020-03-21 12:24:13', '2020-03-21 12:24:13'),
(7, 'Engineer', 1, 0, '2020-03-21 12:24:57', '2020-03-21 12:24:57'),
(8, 'Jr.Engineer', 1, 0, '2020-03-21 12:25:48', '2020-03-21 12:25:48');

-- --------------------------------------------------------

--
-- Table structure for table `users_table`
--

CREATE TABLE `users_table` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `email_id` varchar(255) NOT NULL,
  `user_name` varchar(225) NOT NULL,
  `user_mobile` varchar(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users_table`
--

INSERT INTO `users_table` (`user_id`, `role_id`, `email_id`, `user_name`, `user_mobile`, `dept_id`, `password`, `status`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 8, 'vikas.pandey@aaravsoftware.com', 'vikas pandey', '8169396355', 2, '$2a$08$YHIWKWneaD6ZxGGch/wQGudY8e2D/CCJkGIiL3rA4HZA94jEO78/m', 1, 0, '2020-03-30 12:29:04', '2020-03-30 09:27:11'),
(2, 8, 'vikaspandey319@gmail.com', 'vikash pandey', '8097876819', 1, '$2a$08$YHIWKWneaD6ZxGGch/wQGudY8e2D/CCJkGIiL3rA4HZA94jEO78/m', 2, 0, '2020-03-19 08:13:39', '2020-03-30 12:31:19'),
(3, 8, 'sameet.kotwal@aaravsoftware.com', 'sameets', '8169396356', 4, '$2a$08$d/RyV//aNxSu3ylsTLeIX.erNQoBUMuQt5WSNByIe/jClwFjgD5PS', 1, 0, '2020-03-30 10:30:54', '0000-00-00 00:00:00'),
(4, 2, 'vivek.shukla@aarvsoftware.com', 'vivek Shukla', '8269396355', 1, '$2a$08$2Faq/pT9ePMoKF.5Y51VgObkZBqVS3NPwxhMmKgcGR4nXakW4KPiC', 1, 0, '2020-03-19 12:00:10', '0000-00-00 00:00:00'),
(5, 1, 'raveena.patil@aaravsoftware.com', 'vikas pandey', '8129396356', 1, '$2a$08$9ghM7AKeVgMEN67MFvOlkOCO8Ak9K6htVX9X4bbBu71M5sHJYAaHi', 2, 0, '2020-03-20 06:18:58', '2020-03-30 09:27:16'),
(6, 1, 'ankit.naik@aaravsoftware.com', 'ankit naik', '8169396300', 1, '$2a$08$ak5XSmaJM6wEHPBXBPEi4.P8qwhtVkzxewRnl0dtYiBFOS5CSY83q', 1, 0, '2020-03-30 09:04:36', '0000-00-00 00:00:00'),
(7, 2, 'bijendra.mishra@aaravsoftware.com', 'bijendra mishra', '8754961272', 1, '$2a$08$SKp6VZtrK/BF1XllnYtwsOiShbf6qyPla2qFZfg08uBmfJIk0lVj.', 1, 0, '2020-03-30 09:17:28', '0000-00-00 00:00:00'),
(8, 8, 'prakash.pandey@aaravsoftware.com', 'prakash pandey', '7715990887', 4, '$2a$08$l5ffuZnwvBrV59jWfJLjVeAcgG0GdakFCZtOUhSeGFXSFc0AMm9Xy', 1, 0, '2020-03-30 10:29:51', '2020-03-30 10:04:57');

-- --------------------------------------------------------

--
-- Table structure for table `user_dept_table`
--

CREATE TABLE `user_dept_table` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_dept_table`
--

INSERT INTO `user_dept_table` (`id`, `user_id`, `dept_id`, `status`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 0, '2020-03-30 00:00:00', '2020-03-30 00:00:00'),
(2, 2, 2, 1, 0, '2020-03-30 00:00:00', '2020-03-30 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `applications_details`
--
ALTER TABLE `applications_details`
  ADD PRIMARY KEY (`application_id`);

--
-- Indexes for table `application_remarks`
--
ALTER TABLE `application_remarks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_premssion`
--
ALTER TABLE `app_premssion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_routes`
--
ALTER TABLE `app_routes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `slug` (`slug`);

--
-- Indexes for table `app_status_level`
--
ALTER TABLE `app_status_level`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `auth_sessions`
--
ALTER TABLE `auth_sessions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department_table`
--
ALTER TABLE `department_table`
  ADD PRIMARY KEY (`dept_id`);

--
-- Indexes for table `image_details`
--
ALTER TABLE `image_details`
  ADD PRIMARY KEY (`image_id`);

--
-- Indexes for table `pwd_applications`
--
ALTER TABLE `pwd_applications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `road_type`
--
ALTER TABLE `road_type`
  ADD PRIMARY KEY (`road_id`);

--
-- Indexes for table `roles_table`
--
ALTER TABLE `roles_table`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `users_table`
--
ALTER TABLE `users_table`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_dept_table`
--
ALTER TABLE `user_dept_table`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applications_details`
--
ALTER TABLE `applications_details`
  MODIFY `application_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `application_remarks`
--
ALTER TABLE `application_remarks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `app_premssion`
--
ALTER TABLE `app_premssion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `app_routes`
--
ALTER TABLE `app_routes`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `app_status_level`
--
ALTER TABLE `app_status_level`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `auth_sessions`
--
ALTER TABLE `auth_sessions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `department_table`
--
ALTER TABLE `department_table`
  MODIFY `dept_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `image_details`
--
ALTER TABLE `image_details`
  MODIFY `image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pwd_applications`
--
ALTER TABLE `pwd_applications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `road_type`
--
ALTER TABLE `road_type`
  MODIFY `road_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles_table`
--
ALTER TABLE `roles_table`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users_table`
--
ALTER TABLE `users_table`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user_dept_table`
--
ALTER TABLE `user_dept_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
